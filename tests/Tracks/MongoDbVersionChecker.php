<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\System\MongoDb\Tests\Tracks;

use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;
use BadPixxel\Paddock\Core\Rules\Version;
use BadPixxel\Paddock\System\MongoDb\Collector\VersionCollector;

class MongoDbVersionChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("mongodb-version-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = true;
        $this->description = "[TEST] Check MongoDb Version";
        $this->collector = VersionCollector::getCode();
        $this->rule = Version::getCode();

        //====================================================================//
        // Add Rules
        $this->addRule("version", array(
            "gt" => array("error" => "3.6.0", "warning" => "4.0" ),
            "lt" => array("error" => "6.0.0"),
        ));
    }
}
