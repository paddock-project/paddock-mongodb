<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\System\MongoDb\Tests\Tracks;

use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;
use BadPixxel\Paddock\System\MongoDb\Collector\VariableCollector;

class MongoDbVariablesChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("mongodb-variable-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = true;
        $this->description = "[TEST] Check MongoDb Variables";
        $this->collector = VariableCollector::getCode();

        //====================================================================//
        // Add Rules
        $this->addRule("featureCompatibilityVersion", array(
            "gte" => "4.2",
        ));
    }
}
