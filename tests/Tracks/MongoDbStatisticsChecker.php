<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\System\MongoDb\Tests\Tracks;

use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;
use BadPixxel\Paddock\System\MongoDb\Collector\StatisticsCollector;

class MongoDbStatisticsChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("mongodb-stats-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = true;
        $this->description = "[TEST] Check MongoDb Statictics";
        $this->collector = StatisticsCollector::getCode();

        //====================================================================//
        // Add Rules
        $this->addRule("ok", array("eq" => "1"));
        $this->addRule("scaleFactor", array("gte" => "1"));
    }
}
