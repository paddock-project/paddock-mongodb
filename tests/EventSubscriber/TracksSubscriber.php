<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\System\MongoDb\Tests\EventSubscriber;

use BadPixxel\Paddock\Core\Models\Tracks\AbstractTracksSubscriber;
use BadPixxel\Paddock\System\MongoDb\Tests\Tracks;

class TracksSubscriber extends AbstractTracksSubscriber
{
    /**
     * {@inheritDoc}
     */

    public function getStaticTracks(): array
    {
        return array(
            Tracks\MongoDbVersionChecker::class => array('all' => true),
            Tracks\MongoDbVariablesChecker::class => array('all' => true),
            Tracks\MongoDbStatisticsChecker::class => array('all' => true),
        );
    }
}
