<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

namespace BadPixxel\Paddock\System\MongoDb\Tracks;

use BadPixxel\Paddock\Core\Loader\EnvLoader;
use BadPixxel\Paddock\Core\Models\Tracks\AbstractTrack;
use BadPixxel\Paddock\System\MongoDb\Collector\StatisticsCollector;

class MongoDbHealthChecker extends AbstractTrack
{
    /**
     * Track Constructor
     */
    public function __construct()
    {
        parent::__construct("mongodb-health-checker");
        //====================================================================//
        // Track Configuration
        $this->enabled = !empty(EnvLoader::get("MONGODB_URL"));
        $this->description = "[MongoDb] Check Database Health";
        $this->collector = StatisticsCollector::getCode();

        //====================================================================//
        // Add Rules
        //====================================================================//

        $this->addRule("ok", array("eq" => "1"));

        //====================================================================//
        // Remaining disk free space is above 5G
        $this->addRule("fsFreeSize", array(
            "ne" => true,
            "gte" => array("error" => "1G", "warning" => "5G"),
        ));
        //====================================================================//
        // Remaining disk free space is above 5%
        $this->addRule("fsFreeSizePercent", array(
            "ne" => true,
            "gte" => array("error" => "1", "warning" => "5"),
            "metric" => "mongodb_free",
            "metric-options" => array("uom" => "P"),
        ));
        //====================================================================//
        // Database Used Space
        $this->addRule("fsUsedSizePercent", array(
            "ne" => true,
            "metric" => "mongodb_used",
            "metric-options" => array("uom" => "P"),
        ));
    }
}
