<?php

/*
 *  Copyright (C) BadPixxel <www.badpixxel.com>
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *  For the full copyright and license information, please view the LICENSE
 *  file that was distributed with this source code.
 */

return array(
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => array('all' => true),

    BadPixxel\Paddock\Core\PaddockCoreBundle::class => array('all' => true),
    Knp\Bundle\GaufretteBundle\KnpGaufretteBundle::class => array('all' => true),

    //    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => array('all' => true),
    Doctrine\Bundle\MongoDBBundle\DoctrineMongoDBBundle::class => array('all' => true),

    BadPixxel\Paddock\System\MongoDb\PaddockMongoDbBundle::class => array('all' => true),
    BadPixxel\Paddock\System\MongoDb\Tests\PaddockMongoDbTestBundle::class => array('all' => true),
);
